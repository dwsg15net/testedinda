A APLICA��O FOI FEITA EM C# .NET USANDO O VISUAL STUDIO 2015

Deve abrir a solu��o ConsoleApplication1.sln que est� na raiz do reposit�rio no VISUAL STUDIO, 
se for testar em DEBUG basta ir nas propriedades do projeto > Debug em Command line arguments
e adicionar o nome dos arquivos de teste separados por virgula

Para executar ele usando o Prompt de Comando basta compilar o projeto, ir na pasta
ConsoleApplication1\bin\Debug e executar o programa ConsoleApplication1.exe passando como par�metro
o arquivo de contas com saldo e arquivo de transa��es

Exemplo: ConsoleApplication.exe contas.csv transacoes.csv

Na raiz do projeto existem dois arquivos para teste contas.csv e transacoes.csv, esses arquivos devem
copiados para a pasta do execut�vel.