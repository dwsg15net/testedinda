﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {

        public class Conta
        {

            public Conta(int p_id, int p_saldo)
            {
                this.Id = p_id;
                this.Saldo = p_saldo / 100.00;
            }

            public int Id { get; private set; }
            public double Saldo { get; private set; }

            public void AtualizaSaldo(double valor)
            {
                this.Saldo += valor;

                if(this.Saldo < 0 && valor < 0)
                {
                    this.Saldo -= 5.00;
                }
            }

            public override string ToString()
            {
                return String.Format("{0},{1}", this.Id, (int)(this.Saldo * 100));
            }
        }

        public class Transacao
        {
            public Transacao(int p_id, int p_valor)
            {
                this.IdConta = p_id;
                this.Valor = p_valor / 100.00;
            }

            public int IdConta { get; private set; }
            public double Valor { get; private set; }
        }

        static Dictionary<int, Conta> ExtrairContas(string p_arquivo)
        {

            Dictionary<int, Conta> listaContas = new Dictionary<int, Conta>();

            if (File.Exists(p_arquivo))
            {
                int count = 0;

                using (StreamReader sr = new StreamReader(p_arquivo))
                {
                    while(!sr.EndOfStream)
                    {
                        try
                        {

                            string linha = sr.ReadLine();
                            if (!String.IsNullOrEmpty(linha))
                            {
                                string[] arr = linha.Split(',');

                                int conta;
                                int saldo;

                                if (!int.TryParse(arr[0].Trim(), out conta) || !int.TryParse(arr[1].Trim(), out saldo))
                                {
                                    Console.WriteLine(String.Format("Não foi possível converter os dados da linha {0} do arquivo de contas", count));
                                }
                                else
                                {
                                    listaContas.Add(conta, new Conta(conta, saldo));
                                }
                            }
                            count++;
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(String.Format("Ocorreu um erro desconhecido ao ler a linha {0} do arquivo de contas\nEx: {1}", count, ex.Message));
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("O arquivo de contas não foi encontrado");
            }

            return listaContas;

        }

        static List<Transacao> ExtrairTransacoes(string p_arquivo)
        {

            List<Transacao> listaTransacoes = new List<Transacao>();

            if (File.Exists(p_arquivo))
            {
                int count = 0;

                using (StreamReader sr = new StreamReader(p_arquivo))
                {
                    while (!sr.EndOfStream)
                    {
                        try
                        {

                            string linha = sr.ReadLine();
                            if (!String.IsNullOrEmpty(linha))
                            {
                                string[] arr = linha.Split(',');

                                int conta;
                                int valor;

                                if (!int.TryParse(arr[0].Trim(), out conta) || !int.TryParse(arr[1].Trim(), out valor))
                                {
                                    Console.WriteLine(String.Format("Não foi possível converter os dados da linha {0} do arquivo de transacoes", count));
                                }
                                else
                                {
                                    listaTransacoes.Add(new Transacao(conta, valor));
                                }
                            }
                            count++;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(String.Format("Ocorreu um erro desconhecido ao ler a linha {0} do arquivo de transacoes\nEx: {1}", count, ex.Message));
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("O arquivo de transcoes não foi encontrado");
            }

            return listaTransacoes;

        }

        static void Main(string[] args)
        {
            try
            {
                if (args != null && args.Length == 2)
                {
                    string arquivoContas = args[0];
                    string arquivoTransacoes = args[1];

                    Dictionary<int, Conta> contas = ExtrairContas(arquivoContas);
                    List<Transacao> transacoes = ExtrairTransacoes(arquivoTransacoes);

                    foreach (Transacao transacao in transacoes)
                    {
                        if (contas.ContainsKey(transacao.IdConta))
                        {
                            contas[transacao.IdConta].AtualizaSaldo(transacao.Valor);
                        }
                        else
                        {
                            Console.WriteLine(String.Format("Conta {0} não foi encontrada", transacao.IdConta));
                        }
                    }

                    foreach (Conta conta in contas.Values)
                    {
                        Console.WriteLine(conta.ToString());
                    }
                }
                else
                {
                    Console.WriteLine("Deve ser especificado os dois arquivos");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(String.Format("Ocorreu um erro desconhecido e a operação não pode ser finalizada\nEx: {0}", ex.Message));
            }

            Console.ReadLine();

        }
    }
}
